# Hytek Swim Data Utilities

This project holds small programs that help the LC Barracudas Clerks and Coaches process Hytek swim data export CSVs to pull out specific metrics or insights.

## Inventory

1. Most Improved - Script that determines the swimmers, by age range and gender, max improvement time for any event.


# most-improved

## Pre-Reqs

1. node.js
2. curl

## Running

To run this script:

1. Pull the "Top Times by Names" report from Hytek for the events you want to include, as a CSV.
2. Upload it into a Google Drive location, make it unlisted but public.  Copy the URL.
3. Run this script with `./bin/run.sh URL_COPIED_FROM_GOOGLE`


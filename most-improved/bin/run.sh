#!/bin/bash

# Function to extract the file ID from the Google Drive link
extract_file_id() {
  local url=$1
  # Extract the file ID using a regular expression
  if [[ $url =~ /d/([a-zA-Z0-9_-]+) ]]; then
    echo "${BASH_REMATCH[1]}"
  else
    echo "Invalid Google Drive link."
    exit 1
  fi
}

# Function to download the file using the direct download link
download_file() {
  local file_id=$1
  local output_file=report.csv
  local download_url="https://drive.google.com/uc?export=download&id=${file_id}"
  curl -L -o "${output_file}" "${download_url}"
}

# Main script
if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <google_drive_link>"
  exit 1
fi

google_drive_link=$1
output_file=report.csv

file_id=$(extract_file_id "${google_drive_link}")
download_file "${file_id}" "${output_file}"

echo "File downloaded as ${output_file}"

node ./most-improved.js "${output_file}"

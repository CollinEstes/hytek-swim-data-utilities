const fs = require('fs');
const csv = require('csv-parser');

const CSV_FILE_PATH = process.argv[2];

// Check if the CSV file path is provided as an argument
if (!CSV_FILE_PATH) {
  console.error('Error: CSV file path must be provided as an argument.');
  process.exit(1);
}

// Check if the file exists
if (!fs.existsSync(CSV_FILE_PATH)) {
  console.error(`Error: File '${CSV_FILE_PATH}' does not exist.`);
  process.exit(1);
}

const ACCOUNT_FOR_DATE = true;
const AGE_RANGES = {
  six_under: [0, 6],
  seven_eight: [7, 8],
  nine_ten: [9, 10],
  eleven_twelve: [11, 12],
  thirteen_fourteen: [13, 14],
  fiften_up: [15, 20]
};

/**
 * convertTimeToSeconds - Takes in a Hytek time string and converts to seconds as an int
 * @param {string} time - Format native to Hytek exports ex: "1:36.06Y"
 * @returns {float} - Total seconds
 */
function convertTimeToSeconds(time) {
    const timeParts = time.split(':');
    let seconds = 0;
  
    if (timeParts.length === 2) {
      const minutes = parseInt(timeParts[0]);
      seconds = parseFloat(timeParts[1]);
      seconds += minutes * 60;
    } else if (timeParts.length === 1) {
      seconds = parseFloat(timeParts[0]);
    }
  
    return seconds;
  }

// Function to calculate the improvement time for an event
/**
 * Calculates the total improvement time between two values
 * @param {float} slowestTime 
 * @param {float} fastestTime
 * @returns {float}
 */
function calculateImprovementTime(slowestTime, fastestTime) {
  // ensure floats
  const slowest = parseFloat(slowestTime);
  const fastest = parseFloat(fastestTime);
  // if fastestTime is greater than slowestTest then return a 0 not a negative (shouldn't occur but extra protection)
  return slowest > fastest ? slowest - fastest : 0;
}

// Function to process the CSV file and find the most improved swimmers
function processCSV(CSV_FILE_PATH, ageRange) {
  const swimmers = {}; // this object will hold the data in the shape we build from processes the CSV rows.

  fs.createReadStream(CSV_FILE_PATH)
    .pipe(csv())
    .on('data', (row) => {
      const rowValues = Object.values(row);
      // console.log(row);  // debug line if needed uncomment

      // no data, happens with blank rows
      if (Object.keys(rowValues).length === 0 ) {
        return;
      }

      // pull name and age out of their combined column (thanks Hytek...)
      const nameMatch = rowValues[0].match(/^(.*?) \(\d+\) (\w)$/);
      const ageMatch = rowValues[0].match(/\((\d+)\)/);

      // grab the event date
      const eventDate = new Date(rowValues[7]);

      // when we have a pulled a row cleanly we will have a nameMatch and ageMatch and can proceed
      if (nameMatch && ageMatch) {
        const name = nameMatch[1];
        const age = parseInt(ageMatch[1]);
        const gender = nameMatch[2];

        // Check if the swimmer's age falls within the given range and the gender matches
        if (age >= AGE_RANGES[ageRange][0] && age <= AGE_RANGES[ageRange][1]) {
          const event = rowValues[1];
          let time = rowValues[2].split('Y')[0].trim();
          
          if (time.startsWith('X ')) {
            time = time.substring(2); // Remove the first two characters (the "X " prefix)
          }

          time = convertTimeToSeconds(time);

          // Initialize the swimmer's data if it doesn't exist
          if (!swimmers[name]) {
            swimmers[name] = {
              gender: gender,
              totalImprovement: 0,
              events: {}
            };
          }

          // Update the swimmer's data for the event
          if (!swimmers[name].events[event]) {
            // if we have no data yet for the swimmer and this event then write it
            swimmers[name].events[event] = {
              slowestTime: time,
              fastestTime: time,
              slowestDate: eventDate,
              fastestDate: eventDate
            };
          } else if (ACCOUNT_FOR_DATE) {
            // This would be the second+ time we have this swimmmer/event combo
            // This run is configured to "ACCOUNT FOR DATE" which means the improvement must have occured sequentially in time
            let currentSlowestDate = swimmers[name].events[event].slowestDate
            let currentFastestTime = swimmers[name].events[event].fastestTime


            if (eventDate < currentSlowestDate) {
                swimmers[name].events[event].slowestTime = time
                swimmers[name].events[event].slowestDate = eventDate
            }
            
            if (eventDate > currentSlowestDate && time < currentFastestTime) {
                swimmers[name].events[event].fastestTime = time
                swimmers[name].events[event].fastestDate = eventDate
            }
          } else {
            // This would be the second+ time we have this swimmmer/event combo
            // This run is NOT configured to "ACCOUNT FOR DATE" which means we simply take max/min times and don't care about dates
            swimmers[name].events[event].slowestTime = Math.max(swimmers[name].events[event].slowestTime, time);
            swimmers[name].events[event].fastestTime = Math.min(swimmers[name].events[event].fastestTime, time);
          }
        }
      }
    })
    .on('end', () => {
      // Calculate the total improvement for each swimmer
      Object.keys(swimmers).forEach((name) => {
        const swimmer = swimmers[name];

        // console.log(JSON.stringify(swimmer, null, 2)) // Debug line uncomment as needed
        Object.keys(swimmer.events).forEach((event) => {
          const { slowestTime, fastestTime } = swimmer.events[event];
          swimmer.totalImprovement += calculateImprovementTime(slowestTime, fastestTime);
        });
      });

      // Sort the swimmers by total improvement time
      const sortedSwimmers = Object.entries(swimmers).sort(([, a], [, b]) => b.totalImprovement - a.totalImprovement);

      // Create a new CSV file with the results
      const csvOutput = [['Swimmer Name', 'Gender', 'Total Improvement Time (seconds)']];
      sortedSwimmers.forEach(([name, swimmer]) => {
        csvOutput.push([name, swimmer.gender, swimmer.totalImprovement]);
      });

      const outputFilePath = `./output/results_${ageRange}.csv`;
      fs.writeFileSync(outputFilePath, csvOutput.map(row => row.join(',')).join('\n'));
      console.log(`Results for age range '${ageRange}' have been saved to ${outputFilePath}`);
    });
}

// Process the CSV file for each age range
Object.keys(AGE_RANGES).forEach((ageRange) => {
  processCSV(CSV_FILE_PATH, ageRange);
});
